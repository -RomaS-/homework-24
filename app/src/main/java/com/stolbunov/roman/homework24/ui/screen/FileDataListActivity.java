package com.stolbunov.roman.homework24.ui.screen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.stolbunov.roman.homework24.R;
import com.stolbunov.roman.homework24.data.FileWork;
import com.stolbunov.roman.homework24.ui.adapter.RecyclerViewAdapter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileDataListActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_data_list);

        Intent intent = getIntent();
        String fileName = intent.getStringExtra(MainActivity.class.getName());
        List<String> listDataWithFile = FileWork.getListWithFile(this, fileName);
        initRecyclerView(getRecyclerViewAdapter(listDataWithFile), getLayoutManager());
    }

    @NonNull
    private RecyclerViewAdapter getRecyclerViewAdapter(List<String> listData) {
        return new RecyclerViewAdapter(listData);
    }

    @NonNull
    private RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    private void initRecyclerView(RecyclerViewAdapter adapter, RecyclerView.LayoutManager manager) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }
}
