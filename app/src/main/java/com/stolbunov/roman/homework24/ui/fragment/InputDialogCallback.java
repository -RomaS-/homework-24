package com.stolbunov.roman.homework24.ui.fragment;

public interface InputDialogCallback {
    void onDialogButtonClick(int number);
}
