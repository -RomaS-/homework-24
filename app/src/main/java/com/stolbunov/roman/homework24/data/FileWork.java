package com.stolbunov.roman.homework24.data;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class FileWork {

    public static void writeTextToFile(Context context, String fileName, String text, int numbersLines, int randomNumber) {
        int counter = 0;
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(context.openFileOutput(fileName, MODE_PRIVATE)));
            while (counter < numbersLines) {
                int randomNum = (int) ((Math.random() * randomNumber) + 1);
                bufferedWriter.write(String.format(Locale.ENGLISH, text, randomNum));
                counter++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<String> getListWithFile(Context context, String fileName) {
        BufferedReader reader = null;
        List<String> list = null;
        try {
            reader = new BufferedReader(new InputStreamReader(context.openFileInput(fileName)));
            String str;
            list = new LinkedList<>();
            while (!((str = reader.readLine()) == null)) {
                list.add(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (reader != null)
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
