package com.stolbunov.roman.homework24.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.stolbunov.roman.homework24.R;

public class FragmentDialog extends DialogFragment {
    private AppCompatEditText editText;

    private InputDialogCallback callback;

    public static FragmentDialog getInstance() {
        return new FragmentDialog();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof InputDialogCallback)) {
            throw new IllegalStateException("Implement InputDialogCallback");
        }
        callback = (InputDialogCallback) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof InputDialogCallback)) {
            throw new IllegalStateException("Implement InputDialogCallback");
        }
        callback = (InputDialogCallback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getView(inflater, container);
        setTransparentBackground();
        return view;
    }

    private void setTransparentBackground() {
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    @NonNull
    private View getView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        View view = inflater.inflate(R.layout.fragment_input_num_dialog, container, false);
        AppCompatButton btnEnter = view.findViewById(R.id.fd_btn_enter);
        AppCompatButton btnCancel = view.findViewById(R.id.fd_btn_cancel);
        editText = view.findViewById(R.id.fd_edit_text);
        btnEnter.setOnClickListener(this::onButtonClick);
        btnCancel.setOnClickListener(this::onButtonClick);
        return view;
    }

    private void onButtonClick(View v) {
        switch (v.getId()) {
            case R.id.fd_btn_cancel:
                dismiss();
                break;
            case R.id.fd_btn_enter:
                if (callback != null && editText.getText() != null ) {
                    CharSequence sequence = editText.getText();
                    if (!TextUtils.isEmpty(sequence)) {
                        int number = Integer.parseInt(sequence.toString());
                        callback.onDialogButtonClick(number);
                    }
                }
                dismiss();
                break;
        }
    }
}
