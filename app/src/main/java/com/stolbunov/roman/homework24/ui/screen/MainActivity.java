package com.stolbunov.roman.homework24.ui.screen;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.stolbunov.roman.homework24.R;
import com.stolbunov.roman.homework24.data.FileWork;
import com.stolbunov.roman.homework24.ui.fragment.FragmentDialog;
import com.stolbunov.roman.homework24.ui.fragment.InputDialogCallback;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements InputDialogCallback {
    public static final String FILE_NAME = "file";
    private final int NUMBER_LINES = 10_000;
    private final String FILE_TEXT = "A random number between 0 and X is %d\n";
    private boolean isAlive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatButton button = findViewById(R.id.btn_click_me);
        button.setOnClickListener(this::onStartClick);
    }

    @Override
    public void onDialogButtonClick(int number) {
        SavingDataAsyncTask task = new SavingDataAsyncTask();
        task.execute(number);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isAlive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isAlive = false;
    }

    private void onStartClick(View view) {
        FragmentDialog dialog = FragmentDialog.getInstance();
        dialog.show(getSupportFragmentManager(), "inputNumberDialog");
    }

    class SavingDataAsyncTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... integers) {
            FileWork.writeTextToFile(
                    MainActivity.this,
                    FILE_NAME,
                    FILE_TEXT,
                    NUMBER_LINES,
                    integers[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (MainActivity.this.isAlive) {
                Intent intent = new Intent(MainActivity.this, FileDataListActivity.class);
                intent.putExtra(MainActivity.class.getName(), FILE_NAME);
                startActivity(intent);
            }

        }
    }



}
